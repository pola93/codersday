var gameBackground = {
    createBackground: function() {
        background = game.add.tileSprite(0, 0, game.width, game.height, graphicAssets.background.name);
        background.scale.x = 1 * game.width/100;
        background.scale.y = 5;
        background.autoScroll(0, + 40);
        
        //this.createForeground();
    },
    createPlayerDeathForeground: function(){
        var me = gameState;
        me.back_emitter = game.add.emitter(gameState.mouse.position.x, gameState.mouse.position.y, 25);
        me.back_emitter.makeParticles(graphicAssets.particle.name, [0, 1, 2, 3, 4, 5], 25, false, true);
        me.back_emitter.maxParticleScale = 0.6;
        me.back_emitter.minParticleScale = 0.2;
      //  me.back_emitter.setYSpeed(20, 100);
        me.back_emitter.gravity = 0;
        //me.back_emitter.width = game.world.width ;
        me.back_emitter.minRotation = 0;
        me.back_emitter.maxRotation = 400;
        me.back_emitter.bounce.setTo(0.5, 0.5);
        me.back_emitter.start(false, 0, 20);
    },
    
}

