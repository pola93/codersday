var gameMaster = {
    score: 0,
    spawnBoss: false,
    bossAlive: false,
    scoreText:0,
    bossStartTime : 0,
    update: function () {
        gameMaster.writeScore();
        if (!gameMaster.spawnBoss) {
            enemies.bug.spawn();
            if (gameMaster.score > 1000) {
                enemies.blocker.spawn();
            }
            if (gameMaster.score > 3000) {
                enemies.majorBug.spawn();
            }
            if (gameMaster.score > 8000) {
                enemies.deadline.spawn();
            }
            if (gameMaster.score > 15000) {
                enemies.exception.spawn();
            }
            if (game.time.now > gameMaster.bossStartTime){
                gameMaster.spawnBoss = true;
            }
        }
        else{
            if(!gameMaster.bossAlive){
                gameMaster.setMonstersAway();
                enemies.boss.spawn();
                gameMaster.bossAlive = true;
                
            }
        }
    },
    reset: function() {
        this.score = 0;
        this.spawnBoss=false;
        this.bossAlive= false;
        this.scoreText=0;
    },
    writeScore: function () {
        game.world.remove(gameMaster.scoreText);
        gameMaster.scoreText = game.add.bitmapText(game.width - 550, 30, fontAssets.pixels.name, "SCORE: " + gameMaster.score, 28);
    },
    setMonstersAway: function () {
        enemies.bug.bugGroup.forEachAlive(function (monster) {
            game.physics.arcade.velocityFromAngle(90, enemies.bug.speed*10, monster.body.velocity);
            monster.shotInterval = game.time.now*3;
        });
        enemies.majorBug.majorBugGroup.forEachAlive(function (monster) {
            game.physics.arcade.velocityFromAngle(90, enemies.bug.speed*10, monster.body.velocity);
            monster.shotInterval = game.time.now*3;
        });
        enemies.blocker.blockerGroup.forEachAlive(function (monster) {
            game.physics.arcade.velocityFromAngle(90, enemies.bug.speed*10, monster.body.velocity);
            monster.shotInterval = game.time.now*3;
        });
        enemies.exception.exceptionGroup.forEachAlive(function (monster) {
            game.physics.arcade.velocityFromAngle(90, enemies.bug.speed*10, monster.body.velocity);
            monster.shotInterval = game.time.now*3;
        });
        enemies.deadline.deadlineGroup.forEachAlive(function (monster) {
            game.physics.arcade.velocityFromAngle(90, enemies.bug.speed*10, monster.body.velocity);
            monster.body.collideWorldBounds = false;
            monster.shotInterval = game.time.now*3;
        });
    }
}