var graphicAssets = {
    ship: {URL: 'assets/Nerd.png', name: 'ship', description : 'Thats us, programmer doing all he can against never ending flow of \nerrors and bugs.'},
    ship2: {URL: 'assets/Nerd2.png', name: 'ship2', description : 'Additional screen, second shooting direction.'},
    ship3: {URL: 'assets/Nerd3.png', name: 'ship3', description : 'Two additional screens, trr shooting directions.'},
    bullet: {URL: 'assets/bullet.png', name: 'bullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    enemy: {URL: 'assets/enemy1.png', name: 'enemy', description : 'That\' our enemy, try to destroy it.'},
   
    coffee: {URL: 'assets/Coffe.png', name: 'coffee', description: 'Additional shot of energy from a trusty coffee. Faster shoots.'},
    extraLife: {URL: 'assets/ExtraLife.png', name: 'extraLife', description: 'It\'s good to have a backup. Extra life.'},
    immortality: {URL: 'assets/Immortality.png', name: 'immortality', description: 'Old Nokia. 3 second invincibility.'},
    shield: {URL: 'assets/Shield.png', name: 'shield', description: 'Protects you from one hit.'},
    secondNerd: {URL: 'assets/NewGuy.png', name: 'secondNerd', description: 'Ask a friend to help! Second character appears next to you.'},
    additionalMonitorsPickup: {URL: 'assets/AdditionalMissleDirection.png', name: 'additionalMonitorsPickup', description: 'Gives another screen, and another direction to shoot.'},
    deadline: {URL: 'assets/Deadline.png', name: 'deadline', description: 'Better not let it hit you. Deadline, enough said.'},
    bug: {URL: 'assets/bug.png', name: 'bug', description : 'Classic bug. Easiest enemy.'},
    blocker: {URL: 'assets/Blocker.png', name: 'blocker', description : 'Bug surrounded with a shield.'},
    majorBug: {URL: 'assets/BandytaBug.png', name: 'majorBug', description: 'Major Bug, smarter and harder to kill than normal bug.'},
    particle: {URL: 'assets/enemy1.png', name: 'particle', description : 'Cute spaghetti monster. Don\'t mind it.'},
    exception: {URL: 'assets/Exception.png', name: 'exception', description: 'Exception! Why is it here?! How did it get here?! Defeat it!'},
    boss: {URL: 'assets/Boss.png', name: 'boss', description: 'Try to defeat him to get a rise!'},
    extraMonitor: {URL:'assets/AdditionalMissleDirection.png', name:'extraMonitor', description: 'Gives another screen, and another direction to shoot.'},
    ballShield:{URL: 'assets/BallPickup.png', name: 'ballShield', description: 'Gives you 4 balls that orbit your workstation and blocks 1 hit each.'},
    ball:{URL: 'assets/Ball.png', name: 'ball', description: 'Blocks 1 hit.'},
    
    exceptionBullet:{URL: 'assets/bullets/BulletExc.png', name: 'exceptionBullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    userBullet:{URL: 'assets/bullets/BulletUser.png', name: 'userBullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    bossBullet:{URL: 'assets/bullets/BulletMoney.png', name: 'bossBullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    majorBugBullet:{URL: 'assets/bullets/Bullet.png', name: 'majorBugBullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    blockerBullet:{URL: 'assets/bullets/Bullet2.png', name: 'blockerBullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    bugBullet:{URL: 'assets/bullets/Bullet3.png', name: 'bugBullet', description : 'A missle. If it\'s yours, try to not miss, if it\' yours enemies \n do try to dodge.'},
    background: {URL: 'assets/background.jpg', name:'background', description: ''},
}

var audioAssets = {
    shot: {URL: 'assets/audio/shot.wav', name: 'shot'},
    shortBoom: {URL: 'assets/audio/shortBoom.wav', name: 'shortBoom'},
    bgmMusic: {URL:'assets/audio/menu.wav', name: 'bgmMusic'}
}

var fontAssets = {
    pixels: {URL:'assets/fonts/carrier_command.png', XML:'assets/fonts/carrier_command.xml', name:'pixels'},
}