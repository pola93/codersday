var states = {
    game: "game",
    mainMenu: "mainMenu",
    howToPlayMenu: "howToPlayMenu",
    howToPlayMenu2: "howToPlayMenu2"
};

//var enemyProperties = {
//    startX: gameProperties.screenWidth / 2,
//    startY: 100,
//    onHit: function () {
//       // console.log("Hit!!!");
//    }
//}
var shortBoom;

var gameState = {
    scoreLabel:0,
    music: 0,
    modifier: 0,
    shot: 0,
    mouse: 0,
    fire_button: 0,
    bugGroup: 0,
    exceptionGroup:0,    
    bugInterval:0, 
    deadlineGroup:0,
    pickupSpawned : false,
    isGameOver: false,
    selectedPickup : null,
     cursorSprite: 0,
    pickups : {
        additionalMonitors : additionalMonitorsPickup,
        ballshield : ballShieldPickup,
        coffee : coffeePickup,
        extraLife : extraLifePickup,
        immortality : immortalityPickup,
        secondNerd : secondNerdPickup,
        shield : shieldPickup,
        //slowMotion : slowMotionPickup
    },
    
    activePickups: {},
    
    preload: function () {
        ship.bulletProperties.bulletInterval = 0;
        for(var key in graphicAssets) {
            if(graphicAssets.hasOwnProperty(key)) {
                game.load.image(graphicAssets[key].name, graphicAssets[key].URL);
            }
        }
        
        for(var key in audioAssets) {
            if(audioAssets.hasOwnProperty(key)) {
                game.load.audio(audioAssets[key].name, audioAssets[key].URL);
            }
        }
//        game.load.image(graphicAssets.background.name, graphicAssets.background.URL);
        game.load.bitmapFont(fontAssets.pixels.name, fontAssets.pixels.URL, fontAssets.pixels.XML);
        game.load.spritesheet("resetGame", "assets/resetGame.png");
        game.load.spritesheet("cursor", "assets/cursor.png");
        game.load.atlas('nerdTexture', 'assets/NerdSprite.png', 'assets/json/NerdJson.json');
    },
    init: function() {
        if (ship.HP == 0) {
            this.restart();
        } 
    },
    create: function () {
        this.initGraphics();
        this.initAudio();
        this.initMouse();
        this.initPhysics();
        this.initPauseListener();
        this.music.play();
        
        gameMaster.bossStartTime = game.time.now + Phaser.Timer.SECOND * 90;
        ballShieldPickup.initialize();     
    },
    restart: function() {
        this.music = 0;
        this.modifier=0;
        this.shot= 0;
        this.mouse= 0;
        this.fire_button= 0;
        this.bugGroup= 0;
        this.exceptionGroup=0;    
        this.bugInterval=0;
        this.deadlineGroup=0;
        this.pickupSpawned = false,
        this.isGameOver= false;

       // this.create();
        gameMaster.reset();
        ship.reset();
    },
    update: function () {
       // console.log(ship.HP);
        if (ship.HP == 0 ){
            this.gameOver();
        }
        ship.update();
        this.checkInput();
        this.testCollisions();
        gameMaster.update();
        enemies.update();

        for(var key in this.activePickups) {
            var obj = this.activePickups[key];
            if(obj.pickedUp == false && obj.spawned==false) {
                delete this.activePickups[key];
            }
            else {
                obj.update();
            }
        }
        
        if(!this.pickupSpawned) {
            this.pickupSpawned = true;
            game.time.events.clearPendingEvents();
            game.time.events.add(Phaser.Timer.SECOND * 3, this.spawnNewPickup, this);
        }

        this.cursorSprite.x = this.mouse.pageX - window.innerWidth/4;
        this.cursorSprite.y = this.mouse.pageY;
    },
    render: function () {
        /*game.debug.body(this.enemySprite);
         this.bulletGroup.forEachAlive(function (bullet) {
         game.debug.body(bullet);            
         });*/
    },
    initGraphics: function () {
        gameBackground.createBackground();
        ship.bulletGroup = game.add.group();

        ship.bulletGroup = game.add.group();
    },
    initAudio: function () {
        this.shot = game.add.audio(audioAssets.shot.name);
        shortBoom = game.add.audio(audioAssets.shortBoom.name);
        this.music = game.add.audio(audioAssets.bgmMusic.name, 0.6, true);
    },
    initMouse: function () {
        this.mouse = game.input.activePointer;
        this.fire_button = this.mouse.leftButton;
    },
    initPauseListener: function () {
        pKey = this.input.keyboard.addKey(80);
        pKey.onDown.add(togglePause, this);
    },
    checkInput: function () {
        if (this.fire_button.isDown) {
            this.fire();
        }
    },
    initPhysics: function () {   
        ship.addNerds();
        ship.spawnNewNerd(100,100);
        //inicjalizacja pociskow
        ship.createBullets();
        
        enemies.addEnemies();
    },
    testCollisions: function () {
//        game.physics.arcade.overlap(ship.bulletGroup, exceptionProperties.exceptionGroup, exceptionProperties.checkCollision);
        if(this.selectedPickup != null && this.selectedPickup.spawned == true) {
            this.selectedPickup.checkCollision();
        }

    },
    fire: function () {
        //jeśli możemy już wystrzelić kolejny pocisk
        if (game.time.now > ship.bulletProperties.bulletInterval) {
            //jeśli mamy dostępne pociski, które jeszcze nie zostały stworzone
            //zwraca null jeśli nie mamy dostępnego
            ship.shipGroup.forEachExists(function(item){
                ship.fire(item);
            });
        }
    },
//    enemyCollision: function (bullet, enemy) {
//        bullet.kill();
//        enemyProperties.onHit(enemy);
//        shortBoom.play();
//    },
    gameOver: function () {
       game.world.remove(gameState.scoreLabel);
       gameState.scoreLabel = game.add.text(game.width/2, game.height/2+100, 'Your Score: '+gameMaster.score, { font: '40px Arial', fill: '#fff' });
       gameState.scoreLabel.anchor.setTo(0.5, 0.5);
       if (this.isGameOver) {
           return;
       }
       this.isGameOver = true;
       resetGameBtn = game.add.button(game.width / 2, game.height / 2 + 10, "resetGame", restartLevel);
       resetGameBtn.anchor.setTo(0.5, 0.5);
       resetGameBtn.scale.x = 3;
       resetGameBtn.scale.y = 3;
       
       this.cursorSprite = game.add.sprite(ship.startX, ship.startY, "cursor");
       
        
    },
    spawnNewPickup: function() {
        var keys = Object.keys(this.pickups);
        var pickupNumber = Math.floor((Math.random() * keys.length));
        var selectedPickupKey = Object.keys(this.pickups)[pickupNumber];
        this.selectedPickup = this.pickups[selectedPickupKey];
        
        this.selectedPickup.spawn();
        
        if(!(selectedPickupKey in this.activePickups)) {
            this.activePickups[selectedPickupKey] = this.selectedPickup;
        }
        
    }

};

function togglePause() {
  if (game.paused) {
      game.paused = false;
      pauseLabel.destroy();
  } else {
     game.paused = true;
     pauseLabel = game.add.text(gameProperties.screenWidth/4 , gameProperties.screenHeight/2, 'Game Paused', { font: '80px Arial', fill: '#fff' });
  }
}

function restartLevel () {
    game.state.start('game',true,true);
}

var game = new Phaser.Game(gameProperties.screenWidth, gameProperties.screenHeight, Phaser.AUTO, 'gameDiv');
game.state.add(states.game, gameState);
game.state.add(states.mainMenu, mainMenu);
game.state.add(states.howToPlayMenu, howToPlay);
game.state.add(states.howToPlayMenu2, howToPlayPickups);
game.state.start(states.mainMenu);