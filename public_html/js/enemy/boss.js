enemies.boss = {
    startX: gameProperties.screenWidth / 2,
    startY: 100,
    startHP: 400,
    speed: 600,
    spawnInterval: 0,
    spawnCooldown: 2000,
//    bossShotInterval: 0,
    spawnLimit: 1,
    
    shootLaserFlag: false,
    laserTimer: 0,
    bossLaserShotSpeed: 2500,
    bossLaserShotLength: 1500,
    bossLaserShotCooldown: 5000,
    
    stunTimer: 0,
    bossStunShotSpeed: 2500,
    bossStunShotCooldown: 2000,
    
    normalShotTimer: 0,
    bossNormalShotSpeed: 2500,
    bossNormalShotCooldown: 500,
    
    bossShotSpeed: 2500,
    bossShotCooldown: 1000,
    isFirstTime: true,
    firedBulletsCount: 0,
    bulletRotation : 0,
    bossGroup: 0,
    onHit: function (boss) {
        boss.HP = boss.HP - 1;
        if (boss.HP <= 0) {
            boss.kill();
            ship.HP = 0;
        }
    },
    bossUpdate: function () {
//        enemies.boss.spawn();
        enemies.boss.bossGroup.forEachAlive(function (boss) {
            enemies.boss.checkBoundaries(boss);
            
            if(enemies.boss.shootLaserFlag) {
                enemies.boss.bossShot1(boss);    
            }
            boss.bulletGroup.forEachAlive(function (bullet) {
                enemies.boss.checkBoundaries(bullet);
            });
            game.physics.arcade.overlap(boss.bulletGroup, ship.shipGroup, ship.checkCollision);
            game.physics.arcade.overlap(ship.shipGroup, enemies.boss.bossGroup, enemies.boss.checkCollisionWithPlayer);
        });
        game.physics.arcade.overlap(ship.bulletGroup, enemies.boss.bossGroup, enemies.boss.checkCollisionWithBullet);
    },
    addCreatures: function () {
        enemies.boss.bossGroup = game.add.group();
        enemies.boss.bossGroup.enableBody = true;
        enemies.boss.bossGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.boss.bossGroup.createMultiple(this.spawnLimit, graphicAssets.boss.name);
        enemies.boss.bossGroup.setAll("anchor.x", 0.5);
        enemies.boss.bossGroup.setAll("anchor.y", 0.5);
        
        
        //enemies.boss.stunTimer = game.time.events.loop(enemies.boss.bossStunShotCooldown, enemies.boss.shootStung, this, "shootStun");
        //enemies.boss.normalShotTimer = game.time.events.loop(enemies.boss.bossNormalShotCooldown, enemies.boss.shootNormal, this, "shootNormal");
    },
    checkCollisionWithBullet: function (bullet, enemy) {
        bullet.kill();
        enemies.boss.onHit(enemy);
        shortBoom.play();
    },
    checkCollisionWithPlayer: function (player, enemy) {
        ship.onHit(player);
        shortBoom.play();
    },
    spawn: function () {
        if (!this.isFirstTime) {
            return;
        }
        if (game.time.now > this.spawnInterval) {
            var boss = enemies.boss.bossGroup.getFirstExists(false);
            if (boss) {
                boss.reset(game.rnd.integerInRange(50, game.width - 50), -50);
                boss.HP = enemies.boss.startHP;
                boss.shotInterval = game.time.now + 50;
                boss.body.collideWorldBounds = true;
                boss.body.bounce.setTo(1, 1);
                boss.scale.x = 3;
                boss.scale.y = 3;
                
                boss.bulletGroup = game.add.group();
                boss.bulletGroup.enableBody = true;
                boss.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
                boss.bulletGroup.createMultiple(50, graphicAssets.bossBullet.name);
                boss.bulletGroup.setAll("anchor.x", 0.5);
                boss.bulletGroup.setAll("anchor.y", 0.5);
                boss.bulletGroup.setAll("shotInterval", game.time.now + 2000);
                boss.bulletGroup.setAll("scale.x", 2);
                boss.bulletGroup.setAll("scale.y", 2);

                game.physics.arcade.velocityFromAngle(0, this.speed, boss.body.velocity);
            }
            this.spawnInterval = game.time.now + this.spawnCooldown;
        }
        this.isFirstTime = false;
        enemies.boss.laserTimer = game.time.events.loop(enemies.boss.bossLaserShotCooldown, enemies.boss.shootLaser, this, boss);
        boss.shootLaserFlag = false;
        game.physics.arcade.velocityFromAngle(0, enemies.boss.speed, boss.body.velocity);
    },
    checkBoundaries: function (boss) {
        if (boss.y > game.width) {
            boss.kill();
        }
    },
    bossShot1: function (boss) {
        if (enemies.boss.firedBulletsCount < 50) {
            var bullet = boss.bulletGroup.getFirstExists(false);
            if (bullet) {
                var length = boss.width * 0.5;
                var x = boss.x;
                var y = boss.y + length;
                bullet.reset(x, y);
                if (boss.bulletRotation == undefined || boss.bulletRotation == 0) {
                      boss.bulletRotation = game.physics.arcade.angleBetween(boss, gameState.mouse);
                }
                
                bullet.rotation = boss.bulletRotation;
                game.physics.arcade.velocityFromRotation(bullet.rotation, enemies.boss.bossLaserShotSpeed, bullet.body.velocity);
                enemies.boss.shotInterval = game.time.now + enemies.boss.bossLaserShotLength;
                enemies.boss.firedBulletsCount ++;
                
            }
        } else if (game.time.now > enemies.boss.shotInterval) {
            enemies.boss.bulletRotation = 0;
            enemies.boss.firedBulletsCount = 0;
            game.physics.arcade.velocityFromAngle(0, enemies.boss.speed, boss.body.velocity);
            enemies.boss.shootLaserFlag = false;
        }
        
         
         
        //enemies.exception.exceptionUpdate(boss.x, boss.y );
    },
    
    shootLaser: function(boss) {
        //enemies.boss.stunTimer.stop(false);
       // enemies.boss.normalShotTimer.stop(false);
       game.physics.arcade.velocityFromAngle(0, 0, boss.body.velocity);
        
       enemies.boss.shootLaserFlag = true;
        
        //enemies.boss.speed = 400;
        //enemies.boss.stunTimer.start(0);
        //enemies.boss.normalShotTimer.start(0);
    },
    
    shootStun: function() {
        //enemies.boss.stunTimer.stop(false);
    },
    
    shootNormal: function() {
        //enemies.boss.normalShotTimer.stop(false);
    }
}
