enemies.majorBug = {
    startX: gameProperties.screenWidth / 2,
    startY: 100,
    startHP: 2,
    speed: 100,
    spawnInterval: 0,
    spawnCooldown: 2000,
//    majorBugShotInterval: 0,
    spawnLimit: 5,
    majorBugShotSpeed: 250,
    majorBugShotCooldown: 2000,
    bulletGroup: 0,
    majorBugGroup: 0,
    onHit: function (majorBug) {
        majorBug.HP = majorBug.HP - 1;
        if (majorBug.HP <= 0) {
            majorBug.kill();
            shortBoom.play();
            gameMaster.score = gameMaster.score + 150;
        }
    },
    majorBugUpdate: function () {
//        enemies.majorBug.spawn();
        enemies.majorBug.majorBugGroup.forEachAlive(function (majorBug) {
            enemies.majorBug.checkBoundaries(majorBug);
            enemies.majorBug.majorBugShot(majorBug);
            enemies.majorBug.bulletGroup.forEachAlive(function (bullet) {
                enemies.majorBug.checkBoundaries(bullet);
            });
            game.physics.arcade.overlap(enemies.majorBug.bulletGroup, ship.shipGroup, ship.checkCollision);
            game.physics.arcade.overlap(ship.shipGroup, enemies.majorBug.majorBugGroup, enemies.majorBug.checkCollisionWithPlayer);
        });
        game.physics.arcade.overlap(ship.bulletGroup, enemies.majorBug.majorBugGroup, enemies.majorBug.checkCollisionWithBullet);
    },
    addCreatures: function () {
        enemies.majorBug.majorBugGroup = game.add.group();
        enemies.majorBug.majorBugGroup.enableBody = true;
        enemies.majorBug.majorBugGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.majorBug.majorBugGroup.createMultiple(this.spawnLimit, graphicAssets.majorBug.name);
        enemies.majorBug.majorBugGroup.setAll("anchor.x", 0.5);
        enemies.majorBug.majorBugGroup.setAll("anchor.y", 0.5);

        enemies.majorBug.bulletGroup = game.add.group();
        enemies.majorBug.bulletGroup.enableBody = true;
        enemies.majorBug.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.majorBug.bulletGroup.createMultiple(100, graphicAssets.majorBugBullet.name);
        enemies.majorBug.bulletGroup.setAll("anchor.x", 0.5);
        enemies.majorBug.bulletGroup.setAll("anchor.y", 0.5);
    },
    checkCollisionWithBullet: function (bullet, enemy) {
        bullet.kill();
        enemies.majorBug.onHit(enemy);
    },
    checkCollisionWithPlayer: function (player, enemy) {
        enemy.kill();
        enemies.majorBug.onHit(enemy);
        ship.onHit(player);
        shortBoom.play();
    },
    spawn: function () {
        if (game.time.now > this.spawnInterval) {
            var majorBug = enemies.majorBug.majorBugGroup.getFirstExists(false);
            if (majorBug) {
                majorBug.reset(game.rnd.integerInRange(50, game.width - 50), -50);
                majorBug.HP = enemies.majorBug.startHP;
                majorBug.shotInterval = game.time.now + 750;

                game.physics.arcade.velocityFromAngle(90, this.speed, majorBug.body.velocity);
            }
            this.spawnInterval = game.time.now + this.spawnCooldown;
        }
    },
    checkBoundaries: function (majorBug) {
        if (majorBug.y > game.width) {
            majorBug.kill();
        }
    },
    majorBugShot: function (majorBug) {
        if (game.time.now > majorBug.shotInterval) {
            var bullet = enemies.majorBug.bulletGroup.getFirstExists(false);
            if (bullet) {
                var length = majorBug.width * 0.5;
                var x = majorBug.x;
                var y = majorBug.y + length;
                bullet.reset(x, y);
                bullet.rotation = game.physics.arcade.angleBetween(majorBug, gameState.mouse);

                game.physics.arcade.velocityFromRotation(bullet.rotation, enemies.majorBug.majorBugShotSpeed, bullet.body.velocity);
                majorBug.shotInterval = game.time.now + enemies.majorBug.majorBugShotCooldown;
            }
        }
    }
}
