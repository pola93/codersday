enemies.bug = {
    startX: gameProperties.screenWidth / 2,
    startY: 100,
    startHP: 2,
    speed: 100,
    spawnInterval: 0,
    spawnCooldown: 1500,
//    bugShotInterval: 0,
    spawnLimit: 10,
    bugShotSpeed: 250,
    bugShotCooldown: 2000,
    bulletGroup: 0,
    bugGroup: 0,
    onHit: function (bug) {
        bug.HP = bug.HP - 1;
        if (bug.HP <= 0) {
            bug.kill();
            shortBoom.play();
            gameMaster.score = gameMaster.score + 100;
        }
    },
    bugUpdate: function () {
//        enemies.bug.spawn();
        enemies.bug.bugGroup.forEachAlive(function (bug) {
            enemies.bug.checkBoundaries(bug);
            enemies.bug.bugShot(bug);
            enemies.bug.bulletGroup.forEach(function (bullet) {
                enemies.bug.checkBoundaries(bullet);
            });
            game.physics.arcade.overlap(enemies.bug.bulletGroup, ship.shipGroup, ship.checkCollision);
            game.physics.arcade.overlap(ship.shipGroup, enemies.bug.bugGroup, enemies.bug.checkCollisionWithPlayer);
        });
        game.physics.arcade.overlap(ship.bulletGroup, enemies.bug.bugGroup, enemies.bug.checkCollisionWithBullet);
    },
    addCreatures: function () {
        enemies.bug.bugGroup = game.add.group();
        enemies.bug.bugGroup.enableBody = true;
        enemies.bug.bugGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.bug.bugGroup.createMultiple(this.spawnLimit, graphicAssets.bug.name);
        enemies.bug.bugGroup.setAll("anchor.x", 0.5);
        enemies.bug.bugGroup.setAll("anchor.y", 0.5);

        enemies.bug.bulletGroup = game.add.group();
        enemies.bug.bulletGroup.enableBody = true;
        enemies.bug.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.bug.bulletGroup.createMultiple(100, graphicAssets.bugBullet.name);
        enemies.bug.bulletGroup.setAll("anchor.x", 0.5);
        enemies.bug.bulletGroup.setAll("anchor.y", 0.5);
    },
    checkCollisionWithBullet: function (bullet, enemy) {
        bullet.kill();
        enemies.bug.onHit(enemy);

    },
    checkCollisionWithPlayer: function (player, enemy) {
        enemy.kill();
        enemies.bug.onHit(enemy);
        ship.onHit(player);
        shortBoom.play();
    },
    spawn: function () {
        if (game.time.now > this.spawnInterval) {
            var bug = enemies.bug.bugGroup.getFirstExists(false);
            if (bug) {
                bug.reset(game.rnd.integerInRange(50, game.width - 50), -50);
                bug.HP = enemies.bug.startHP;
                bug.shotInterval = game.time.now + 750;

                game.physics.arcade.velocityFromAngle(90, this.speed, bug.body.velocity);
            }
            this.spawnInterval = game.time.now + this.spawnCooldown;
        }
    },
    checkBoundaries: function (bug) {
        if (bug.y > game.width) {
            bug.kill();
        }
    },
    bugShot: function (bug) {
        if (game.time.now > bug.shotInterval) {
            var bullet = enemies.bug.bulletGroup.getFirstExists(false);
            if (bullet) {
                var length = bug.width * 0.5;
                var x = bug.x;
                var y = bug.y + length;
                bullet.reset(x, y);
                bullet.rotation = 90 * Math.PI / 180;

                game.physics.arcade.velocityFromRotation(bullet.rotation, enemies.bug.bugShotSpeed, bullet.body.velocity);
                bug.shotInterval = game.time.now + enemies.bug.bugShotCooldown;
            }
        }
    }
}