/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
enemies.addEnemies = function () {
    enemies.bug.addCreatures();
    enemies.blocker.addCreatures();
    enemies.exception.addCreatures();
    enemies.majorBug.addCreatures();
    enemies.deadline.addCreatures();
    enemies.boss.addCreatures();
};

enemies.update = function () {
    enemies.bug.bugUpdate();
    enemies.blocker.blockerUpdate();
    enemies.exception.exceptionUpdate();
    enemies.majorBug.majorBugUpdate();
    enemies.deadline.deadlineUpdate();
    enemies.boss.bossUpdate();
}

