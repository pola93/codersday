enemies.deadline = {
    startX: gameProperties.screenWidth / 2,
    startY: 100,
    startHP: 2,
    speed: 100,
    bulletGroup: 0,
    spawnInterval: 0,
    spawnCooldown: 2000,
    deadlineShotInterval: 0,
    spawnLimit: 5,
    deadlineShotSpeed: 1500,
    deadlineShotCooldown: 4000,
    firedBulletsCount: 0,
    deadlineGroup: 0,
    onHit: function (deadline) {
        deadline.HP = deadline.HP - 1;
        if (deadline.HP <= 0) {
            deadline.kill();
            gameMaster.score = gameMaster.score + 500;
        }
    },
    deadlineUpdate: function () {
//        enemies.deadline.spawn();

        enemies.deadline.deadlineGroup.forEachAlive(function (deadline) {
            enemies.deadline.checkBoundaries(deadline);
            enemies.deadline.deadlineShot(deadline);
            enemies.deadline.bulletGroup.forEachAlive(function (bullet) {
                enemies.deadline.checkBoundaries(bullet);
            });
            game.physics.arcade.overlap(enemies.deadline.bulletGroup, ship.shipGroup, ship.checkCollision);
            game.physics.arcade.overlap(gameState.shipGroup, enemies.deadline.deadlineGroup, enemies.deadline.checkCollisionWithPlayer);
        });
        game.physics.arcade.overlap(ship.bulletGroup, enemies.deadline.deadlineGroup, enemies.deadline.checkCollisionWithPlayer);
    },
    addCreatures: function () {
        enemies.deadline.deadlineGroup = game.add.group();
        enemies.deadline.deadlineGroup.enableBody = true;
        enemies.deadline.deadlineGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.deadline.deadlineGroup.createMultiple(this.spawnLimit, graphicAssets.deadline.name);
        enemies.deadline.deadlineGroup.setAll("anchor.x", 0.5);
        enemies.deadline.deadlineGroup.setAll("anchor.y", 0.5);

        enemies.deadline.bulletGroup = game.add.group();
        enemies.deadline.bulletGroup.enableBody = true;
        enemies.deadline.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.deadline.bulletGroup.createMultiple(250, graphicAssets.bullet.name);
        enemies.deadline.bulletGroup.setAll("anchor.x", 0.5);
        enemies.deadline.bulletGroup.setAll("anchor.y", 0.5);
    },
    checkCollisionWithBullet: function (bullet, deadline) {
        bullet.kill();
        enemies.deadline.onHit(deadline);
        shortBoom.play();
    },
    checkCollisionWithPlayer: function (player, enemy) {
        enemies.deadline.onHit(enemy);
        ship.onHit(player);
        shortBoom.play();
    },
    spawn: function () {
        if (game.time.now > this.spawnInterval) {
            var deadline = enemies.deadline.deadlineGroup.getFirstExists(false);
            if (deadline) {
                deadline.reset(game.rnd.integerInRange(50, game.width - 50), 50);
                deadline.HP = enemies.deadline.startHP;
                deadline.shotInterval = game.time.now + 750;
                deadline.body.collideWorldBounds = true;
                deadline.body.bounce.setTo(1, 1);

                game.physics.arcade.velocityFromAngle(0, this.speed, deadline.body.velocity);
            }
            this.spawnInterval = game.time.now + this.spawnCooldown;
        }
    },
    checkBoundaries: function (deadline) {
        if (deadline.y > game.width) {
            deadline.kill();
        }
    },
    deadlineShot: function (deadline) {
        if (deadline.firedBulletsCount < 50) {
            var bullet = enemies.deadline.bulletGroup.getFirstExists(false);
            if (bullet) {
                var length = deadline.width * 0.5;
                var x = deadline.x;
                var y = deadline.y + length;

                bullet.reset(x, y);
                bullet.rotation = 90 * Math.PI / 180;
                game.physics.arcade.velocityFromRotation(bullet.rotation, enemies.deadline.deadlineShotSpeed, bullet.body.velocity);
                deadline.shotInterval = game.time.now + enemies.deadline.deadlineShotCooldown;
                deadline.firedBulletsCount++;
            }
        } else if (game.time.now > deadline.shotInterval) {
            deadline.isFired = false;
            deadline.firedBulletsCount = 0;
        }
    }
}
