enemies.blocker = {
    startX: gameProperties.screenWidth / 2,
    startY: 100,
    startHP: 5,
    speed: 100,
    spawnInterval: 0,
    spawnCooldown: 2000,
    bulletGroup: 0,
//    bugShotInterval: 0,
    spawnLimit: 5,
    blockerShotSpeed: 200,
    blockerShotCooldown: 2000,
    blockerGroup: 0,
    onHit: function (blocker) {
        blocker.HP = blocker.HP - 1;
        if (blocker.HP <= 0) {
            blocker.kill();
            shortBoom.play();
            gameMaster.score = gameMaster.score + 200;
        }
    },
    blockerUpdate: function () {
//        enemies.blocker.spawn();
        enemies.blocker.blockerGroup.forEachAlive(function (blocker) {
            enemies.blocker.checkBoundaries(blocker);
            enemies.blocker.blockerShot(blocker);
            enemies.blocker.bulletGroup.forEachAlive(function (bullet) {
                enemies.blocker.checkBoundaries(bullet);
            });
            game.physics.arcade.overlap(enemies.blocker.bulletGroup, ship.shipGroup, ship.checkCollision);
            game.physics.arcade.overlap(ship.shipGroup, enemies.blocker.blockerGroup, enemies.blocker.checkCollisionWithPlayer);
        });
        game.physics.arcade.overlap(ship.bulletGroup, enemies.blocker.blockerGroup, enemies.blocker.checkCollisionWithBullet);
    },
    addCreatures: function () {
        enemies.blocker.blockerGroup = game.add.group();
        enemies.blocker.blockerGroup.enableBody = true;
        enemies.blocker.blockerGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.blocker.blockerGroup.createMultiple(this.spawnLimit, graphicAssets.blocker.name);
        enemies.blocker.blockerGroup.setAll("anchor.x", 0.5);
        enemies.blocker.blockerGroup.setAll("anchor.y", 0.5);

        enemies.blocker.bulletGroup = game.add.group();
        enemies.blocker.bulletGroup.enableBody = true;
        enemies.blocker.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.blocker.bulletGroup.createMultiple(100, graphicAssets.blockerBullet.name);
        enemies.blocker.bulletGroup.setAll("anchor.x", 0.5);
        enemies.blocker.bulletGroup.setAll("anchor.y", 0.5);
    },
    checkCollisionWithBullet: function (bullet, enemy) {
        bullet.kill();
        enemies.blocker.onHit(enemy);
    },
    checkCollisionWithPlayer: function (player, enemy) {
        enemy.kill();
        enemies.blocker.onHit(enemy);
        ship.onHit(player);
        shortBoom.play();
    },
    spawn: function () {
        if (game.time.now > this.spawnInterval) {
            var blocker = enemies.blocker.blockerGroup.getFirstExists(false);
            if (blocker) {
                blocker.reset(game.rnd.integerInRange(50, game.width - 50), -50);
                blocker.HP = enemies.blocker.startHP;
                blocker.shotInterval = game.time.now + 750;
                game.physics.arcade.velocityFromAngle(90, this.speed, blocker.body.velocity);
            }
            this.spawnInterval = game.time.now + this.spawnCooldown;
        }
    },
    checkBoundaries: function (blocker) {
        if (blocker.y > game.width) {
            blocker.kill();
        }
    },
    blockerShot: function (blocker) {
        if (game.time.now > blocker.shotInterval) {
            var bullet = enemies.blocker.bulletGroup.getFirstExists(false);
            if (bullet) {
                var length = blocker.width * 0.5;
                var x = blocker.x;
                var y = blocker.y + length;
                bullet.reset(x, y);
                bullet.rotation = 90 * Math.PI / 180;

                game.physics.arcade.velocityFromRotation(bullet.rotation, enemies.blocker.blockerShotSpeed, bullet.body.velocity);
                blocker.shotInterval = game.time.now + enemies.blocker.blockerShotCooldown;
            }
        }
    }
}