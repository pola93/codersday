enemies.exception = {
    startX: gameProperties.screenWidth / 2,
    startY: 100,
    startHP: 2,
    speed: 100,
    bulletGroup:0,
    spawnInterval: 0,
    spawnCooldown: 2000,
    spawnLimit: 5,
    exceptionShotSpeed: 350,
    exceptionShotCooldown: 2000,
    exceptionGroup: 0,
    onHit: function (exception) {
        exception.HP = exception.HP - 1;
        if (exception.HP <= 0) {
            exception.kill();
            gameMaster.score = gameMaster.score + 250;
            shortBoom.play();
        }
    },
    exceptionUpdate: function (possitionX, possitionY) {
        enemies.exception.exceptionGroup.forEachAlive(function (exception) {
            enemies.exception.checkBoundaries(exception);
            enemies.exception.exceptionShot(exception);
            enemies.exception.bulletGroup.forEachAlive(function (bullet) {
                enemies.exception.checkBoundaries(bullet);
            });
            game.physics.arcade.overlap(enemies.exception.bulletGroup, ship.shipGroup, ship.checkCollision);
            game.physics.arcade.overlap(ship.shipGroup, enemies.exception.exceptionGroup, enemies.exception.checkCollisionWithPlayer);
        });
        game.physics.arcade.overlap(ship.bulletGroup, enemies.exception.exceptionGroup, enemies.exception.checkCollisionWithBullet);
    },
    addCreatures: function () {
        enemies.exception.exceptionGroup = game.add.group();
        enemies.exception.exceptionGroup.enableBody = true;
        enemies.exception.exceptionGroup.physicsBodyType = Phaser.Physics.ARCADE;
        enemies.exception.exceptionGroup.createMultiple(this.spawnLimit, graphicAssets.exception.name);
        enemies.exception.exceptionGroup.setAll("anchor.x", 0.5);
        enemies.exception.exceptionGroup.setAll("anchor.y", 0.5);
        
        enemies.exception.bulletGroup = game.add.group();
                enemies.exception.bulletGroup.enableBody = true;
                enemies.exception.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
                enemies.exception.bulletGroup.createMultiple(100, graphicAssets.exceptionBullet.name);
                enemies.exception.bulletGroup.setAll("anchor.x", 0.5);
                enemies.exception.bulletGroup.setAll("anchor.y", 0.5);
//        game.add.tween(enemies.exception.exceptionGroup.position).to({x: 100}, 1000, Phaser.Easing.Back.Out, true, 2000, -1, true);
    },
    checkCollisionWithBullet: function (bullet, enemy) {
        bullet.kill();
        enemies.exception.onHit(enemy); 
    },
    checkCollisionWithPlayer: function (player, enemy) {
        enemy.kill();
        enemies.exception.onHit(enemy);
        ship.onHit(player, true);
        shortBoom.play();
    },
    spawn: function (possitionX, possitionY) {
        if (game.time.now > this.spawnInterval) {
            var exception = enemies.exception.exceptionGroup.getFirstExists(false);
            if (exception) {
                if (possitionX == undefined || possitionY == undefined) {
                    exception.reset(game.rnd.integerInRange(50, game.width - 50), -50);
                } else {
                    exception.reset(possitionX, possitionY);
                }
                
                game.add.tween(exception.position).to({x: 100}, 1500, Phaser.Easing.Back.Out, true, 0, -1, true);
                exception.HP = enemies.exception.startHP;
                exception.shotInterval = game.time.now + 750;

                game.physics.arcade.velocityFromAngle(90, this.speed, exception.body.velocity);
            }
            this.spawnInterval = game.time.now + this.spawnCooldown;
        }
    },
    checkBoundaries: function (bug) {
        if (bug.y > game.width) {
            bug.kill();
        }
    },
    exceptionShot: function (exc) {
        if (game.time.now > exc.shotInterval) {
            var exceptionBullet = enemies.exception.bulletGroup.getFirstExists(false);
            if (exceptionBullet) {
                var length = exc.width * 0.5;
                var x = exc.x;
                var y = exc.y + length;
//                console.log("z shota")
//                console.log(x)
//                console.log(y)
                exceptionBullet.reset(x, y);
                exceptionBullet.rotation = 90 * Math.PI / 180;

                game.physics.arcade.velocityFromRotation(exceptionBullet.rotation, enemies.exception.exceptionShotSpeed, exceptionBullet.body.velocity);
                exc.shotInterval = game.time.now + enemies.exception.exceptionShotCooldown;
            }
        }
    }
}
