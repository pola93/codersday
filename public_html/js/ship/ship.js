var ship = {
    name: 'ship',
    startX: gameProperties.screenWidth / 2,
    startY: gameProperties.screenHeight / 2,
    HP: 3,
    hpGraphicsGroup: 0,
    sprite: 0,
    numberOfNerds: 0,
    shield: 0,
    numberOfMonitors: 1,
    bulletInterval: 200,
    modifier: 0,
    stun: false,
    stunDuration: 1,
    shipIsInvulnerable: false,
    blinkDelay: 0.2,
    timeToReset: 3,
    shipGroup: 0,
    bulletGroup: 0,
    bulletProperties: {
        speed: 400,
        interval: 0,
        lifespan: 2000
    },
    reset : function() {
        this.HP = 3;
        this.sprite= 0;
        this.numberOfNerds =0;
        this.shield = 0;
        this.numberOfMonitors = 1;
        this.bulletInterval= 200;
        this.modifier= 0;
        this.stun= false;
        this.stunDuration= 1;
        this.shipIsInvulnerable= false;
        this.blinkDelay=0.2;
        this.timeToReset= 3;
        this.shipGroup= 0;
        this.bulletGroup= 0;
        this.bulletProperties= {
            speed: 400,
            interval: 0,
            lifespan: 2000
        };
    },
    update: function () {
        ship.modifier = ship.numberOfNerds - 1;
        var cetrializer = 0;
        
        switch (ship.modifier) {
                    case 1: {
                            cetrializer = -32;
                            break;
                    }
                    case 2: {
                            cetrializer = -64;
                            break;
                    }

                }
        
        ship.shipGroup.forEachExists(function(item){
            if(!ship.stun){                
                item.x = gameState.mouse.position.x + cetrializer;
                item.y = gameState.mouse.position.y;
                cetrializer+=64;
            }
            else {
                item.x = item.x
                item.y = item.y;
            }
            ship.modifier++;
        }, gameState);
    },
    onHit: function (player, exception) {
        if (!ship.shipIsInvulnerable) {
            if (exception !== undefined && exception === true) {
                ship.stun = true;
                game.time.events.add(Phaser.Timer.SECOND * ship.stunDuration, ship.toggleStun, this);
            }
            else {
                if (ship.shield > 0) {
                    shieldPickup.restoreShield();
                    ship.shield--;
                }
                else if (ship.numberOfNerds > 1) {
                    ship.numberOfNerds--;
                    player.kill();
                }
                else if (ship.numberOfMonitors > 1) {
                    ship.numberOfMonitors--;
                    switch(ship.numberOfMonitors){
                        case 1:
                            ship.shipGroup.forEach(function (item) {
                                item.frameName = 'NerdSprite00';
                            });
                            break;
                        case 2:
                            ship.shipGroup.forEach(function (item) {
                                item.frameName = 'NerdSprite01';
                            });
                            break;
                        case 3:
                            ship.shipGroup.forEach(function (item) {
                                item.frameName = 'NerdSprite02';
                            });
                            break;
                    }
                }
                else {
                    ship.HP--;
                    ship.shipIsInvulnerable = true;
                    gameState.time.events.repeat(Phaser.Timer.SECOND * ship.blinkDelay, ship.timeToReset / ship.blinkDelay, ship.shipBlink, gameState);
                    gameState.time.events.add(Phaser.Timer.SECOND * ship.timeToReset, ship.shipReady, gameState);
                    
                    var width = gameProperties.screenWidth;
                    ship.hpGraphicsGroup.forEach(function(item) {
                        item.kill();
                    });
                    
                    for(var i = 1; i <= ship.HP; i++){
                        var hpGraph = ship.hpGraphicsGroup.getFirstExists(false);

                        if (hpGraph) {
                            hpGraph.reset(gameProperties.screenWidth - 100 * i, 100);
                        }
                    }
                }

                if (ship.HP < 1) {
                    player.kill();
                    gameBackground.createPlayerDeathForeground();
                    ballShieldPickup.destroy();
                }
            }
        }
//        console.log(ship.HP)
    },
    checkCollision: function (bug, player) {
        bug.kill();
        ship.onHit(player);
        shortBoom.play();
    },
    addNerds: function () {
        ship.shipGroup = game.add.group();
        ship.shipGroup.enableBody = true;
        ship.shipGroup.physicsBodyType = Phaser.Physics.ARCADE;
        ship.shipGroup.createMultiple(3, 'nerdTexture', 'NerdSprite01');
        ship.shipGroup.setAll("anchor.x", 0.5);
        ship.shipGroup.setAll("anchor.y", 0.5);
        ship.shipGroup.setAll("rotation", -90 * (Math.PI / 180));

        ship.shipGroup.forEach(function (item) {
            item.frameName = 'NerdSprite00';
        });
        
        ship.hpGraphicsGroup = game.add.group();
        ship.hpGraphicsGroup.createMultiple(5, graphicAssets.extraLife.name);
        ship.hpGraphicsGroup.setAll("anchor.x", 0.5);
        ship.hpGraphicsGroup.setAll("anchor.y", 0.5);
        ship.hpGraphicsGroup.setAll("rotation", -90 * (Math.PI / 180));
        
        for(var i = 1; i <= ship.HP; i++){
            var hpGraph = ship.hpGraphicsGroup.getFirstExists(false);

            if (hpGraph) {
                hpGraph.reset(gameProperties.screenWidth - 100 * i, 100);
            }
        }
    },
    addHpGraphics: function (){
        
    },
    spawnNewNerd: function (x, y) {
        var newNerd = ship.shipGroup.getFirstExists(false);
        if (newNerd != null) {
            newNerd.reset(x, y);
            newNerd.rotation = -90 * (Math.PI / 180);
            ship.numberOfNerds++;
        }
    },
    createBullets: function () {
        //fizyka dla pocisków
        this.bulletGroup.enableBody = true;
        this.bulletGroup.physicsBodyType = Phaser.Physics.ARCADE;
        //tworzenie grupy pocisków, max 50 instancji na raz może istnieć
        this.bulletGroup.createMultiple(500, graphicAssets.userBullet.name);
        this.bulletGroup.setAll("anchor.x", 0.5);
        this.bulletGroup.setAll("anchor.y", 0.5);
        this.bulletGroup.setAll("lifespan", this.bulletProperties.lifespan);
        this.bulletGroup.forEach(function (bullet) {
            bullet.body.setSize(24, 40, 0, 0);
        });
    },
    fire: function (shipContext) {
        var bullet = this.bulletGroup.getFirstExists(false);
        if (bullet) {
            var bullet;
            var rotation;
            for (var i = 0; i < ship.numberOfMonitors; i++) {
                bullet = this.bulletGroup.getFirstExists(false);
                if (bullet) {
                    var length = shipContext.width * 0.5;
                    var x = shipContext.x + (Math.cos(shipContext.rotation) * length);
                    var y = shipContext.y + (Math.sin(shipContext.rotation) * length);

                    bullet.reset(x, y);
                    bullet.lifespan = ship.bulletProperties.lifespan;

                    //ustalenie prędkości pocisku;
                    rotation = shipContext.rotation;
                    if (i == 1) {
                        rotation += 45 * Math.PI / 180;
                    }
                    if (i == 2) {
                        rotation -= 45 * Math.PI / 180;
                    }
                    bullet.rotation = rotation;
                    game.physics.arcade.velocityFromRotation(rotation, this.bulletProperties.speed, bullet.body.velocity);
                    ship.bulletProperties.bulletInterval = gameState.game.time.now + this.bulletInterval;
                }
            }
            gameState.shot.play();
        }
    },
    toggleStun: function () {
        ship.stun = !ship.stun;
    },
    shipBlink: function () {
        ship.shipGroup.forEachExists(function (item) {
            item.visible = !item.visible;
        });
    },
    shipReady: function () {
        ship.shipIsInvulnerable = false;
        ship.shipGroup.forEachExists(function (item) {
            item.visible = true;
        });
    }
};