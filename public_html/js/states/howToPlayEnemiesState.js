

var howToPlay = {
    preload: function () {
        game.load.image("undoButton", "assets/undoButton.png");
        game.load.image("nextButton", "assets/nextButton.png");

        for (var key in graphicAssets) {
            if (graphicAssets.hasOwnProperty(key)) {
                game.load.image(graphicAssets[key].name, graphicAssets[key].URL);
            }
        }
    },
    create: function () {
        var i = 60;
        var keysToShow = [graphicAssets.bug.name, graphicAssets.blocker.name, graphicAssets.majorBug.name, graphicAssets.deadline.name, graphicAssets.exception.name];
        for (var j = 0; j < keysToShow.length; j++) {
            if (graphicAssets[keysToShow[j]]) {
                // if (graphicAssets[key].name.toString().indexOf("enemy") != -1) {
                var enemy = game.add.sprite(game.width / 10, i, graphicAssets[keysToShow[j]].name);
                enemy.anchor.set(0.5);
                game.add.text(game.width / 10 + 40, i, graphicAssets[keysToShow[j]].description, {font: gameProperties.screenWidth / 40 + 'px Arial', fill: '#fff'});

                i = i + 100;
                //  }               
            }
        }

        var undoButton = game.add.button(100, game.height - 50, "undoButton", howToPlay.undoButtonClick);
        undoButton.anchor.set(0.5);
        var nextButton = game.add.button(game.width - 100, game.height - 50, "nextButton", howToPlay.nextButtonClick);
        nextButton.anchor.set(0.5);
        this.initMouse();
        this.cursorSprite = game.add.sprite(ship.startX, ship.startY, "cursor");
    },
    update: function () {
        this.cursorSprite.x = this.mouse.pageX - window.innerWidth / 4;
        this.cursorSprite.y = this.mouse.pageY;
    },
    initMouse: function () {
        this.mouse = game.input.activePointer;
    },
    undoButtonClick: function () {
        game.state.start(states.mainMenu);
    },
    nextButtonClick: function () {
        game.state.start(states.howToPlayMenu2);
    }

}

