

var mainMenu = {
    cursorSprite: 0,
     preload: function () {
        game.stage.backgroundColor = "#020028";
        game.load.spritesheet("gametitle", "assets/Title.png");
        game.load.spritesheet("gridedition", "assets/gridedition.png");
        game.load.spritesheet("playbutton", "assets/playbutton.png");
        game.load.spritesheet("howToPlay", "assets/howTo.png");
        game.load.spritesheet("menubutton", "assets/menubutton.png");
        game.load.spritesheet("resetgame", "assets/resetgame.png");
        game.load.spritesheet("thankyou", "assets/thankyou.png");
        game.load.spritesheet("cursor", "assets/cursor.png");

    },
    create: function () {
        var title = game.add.sprite(game.width / 2, 100, "gametitle");
        title.anchor.set(0.5); 
        var grid = game.add.sprite(game.width / 2, 250, "gridedition");
        grid.anchor.set(0.5);
        var playButton = game.add.button(game.width / 2, game.height / 2 + 70, "playbutton", playBtnClick);
        playButton.anchor.set(0.5);
        var howToPlayButton = game.add.button(game.width / 2, game.height / 2 +250, "howToPlay", howToPlayBtnClick);
        howToPlayButton.anchor.set(0.5);
 
        menuGroup = game.add.group();
        
        var menuButton = game.add.button(game.width / 2, game.height - 30, "menubutton", toggleMenu);
        menuButton.anchor.set(0.5);
        menuGroup.add(menuButton);
        var resetGame = game.add.button(game.width / 2, game.height + 50, "resetgame", function(){});
        resetGame.anchor.set(0.5);
        menuGroup.add(resetGame);
        var thankYou = game.add.button(game.width / 2, game.height + 130, "thankyou", function(){});
        thankYou.anchor.set(0.5);
        menuGroup.add(thankYou);     
        
        this.initMouse();
        this.cursorSprite = game.add.sprite(ship.startX, ship.startY, "cursor");
    },
    update: function() {
        this.cursorSprite.x = this.mouse.pageX - window.innerWidth/4;
        this.cursorSprite.y = this.mouse.pageY;
    },
    initMouse: function () {
        this.mouse = game.input.activePointer; 
    }
}

function playBtnClick () {
    game.state.start(states.game);           
}

function howToPlayBtnClick() {
    game.state.start(states.howToPlayMenu);           
}

function toggleMenu(){
     if(menuGroup.y == 0){
          var menuTween = game.add.tween(menuGroup).to({
               y: -180     
          }, 500, Phaser.Easing.Bounce.Out, true);
     }
     if(menuGroup.y == -180){
          var menuTween = game.add.tween(menuGroup).to({
               y: 0    
          }, 500, Phaser.Easing.Bounce.Out, true);     
     }
}
