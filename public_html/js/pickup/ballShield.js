/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var ballShieldPickup = {
    name: 'ballShield',
    startX: 0,
    startY: 0,
    duration: -1,
    graphicURL: graphicAssets.ballShield.name,
    sprite: 0,
    HP: 0,
    DMG: 99,
    shieldGroup: 0,
    rotationSpeed: 100,
    xpivot: 100,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() {
        
        ballShieldPickup.destroy();
        
        ballShieldPickup.pickedUp = true;
        gameState.pickupSpawned = false;
        
        ballShieldPickup.shieldGroup = game.add.group();
        ballShieldPickup.shieldGroup.enableBody = true;
        ballShieldPickup.shieldGroup.physicsBodyType = Phaser.Physics.ARCADE;
        ballShieldPickup.HP=4;
        

        for(var i = 0; i < 4; i++) {
            ballShieldPickup.shieldGroup.create(gameState.mouse.position.x, gameState.mouse.position.y, graphicAssets.ball.name);
        }
        ballShieldPickup.shieldGroup.setAll("anchor.x", 0.5);
        ballShieldPickup.shieldGroup.setAll("anchor.y", 0.5);
        
        ballShieldPickup.shieldGroup.getAt(0).pivot.x = 100;
        ballShieldPickup.shieldGroup.getAt(1).pivot.x = -100;
        ballShieldPickup.shieldGroup.getAt(2).pivot.y = 100;
        ballShieldPickup.shieldGroup.getAt(3).pivot.y = -100;
        
        ballShieldPickup.shieldGroup.forEach(function (ballShield) {
            ballShield.body.setSize(57, 47, 2, 5); 
        });
        ballShieldPickup.sprite.kill();
    },
    

    spawn : function () {
        ballShieldPickup.spawned = true;
        ballShieldPickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, ballShieldPickup.graphicURL);
        game.physics.arcade.enable(ballShieldPickup.sprite, Phaser.Physics.ARCADE);
        ballShieldPickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, ballShieldPickup.sprite.body.velocity);

    },
    
    initialize : function() {
        ballShieldPickup.shieldGroup = game.add.group();
        ballShieldPickup.shieldGroup.enableBody = true;
        ballShieldPickup.shieldGroup.physicsBodyType = Phaser.Physics.ARCADE;
    },
    
    onHitEnemy : function(enemy, shieldBullet) {
        ballShieldPickup.HP--;
        enemy.kill();
        shieldBullet.kill();
        shortBoom.play();
        if(ballShieldPickup.shieldGroup.countLiving() == 0) {
            ballShieldPickup.pickedUp = false;
        }
    },
    
    destroy : function() {
        ballShieldPickup.shieldGroup.forEach(function (ballShield) {
            ballShield.kill();
        });
    },
    
    update : function() {

        ballShieldPickup.shieldGroup.forEach(function (shieldBall) {
            shieldBall.rotation += 0.05;
            shieldBall.x = gameState.mouse.position.x;
            shieldBall.y = gameState.mouse.position.y;
        });
        game.physics.arcade.overlap(enemies.bug.bugGroup, ballShieldPickup.shieldGroup, ballShieldPickup.onHitEnemy);
        game.physics.arcade.overlap(enemies.blocker.blockerGroup, ballShieldPickup.shieldGroup, ballShieldPickup.onHitEnemy);
        //game.physics.arcade.overlap(enemies.deadline.deadlineGroup, ballShieldPickup.shieldGroup, ballShieldPickup.onHitEnemy);
        //game.physics.arcade.overlap(enemies.exception.exceptionGroup, ballShieldPickup.shieldGroup, ballShieldPickup.onHitEnemy);
        game.physics.arcade.overlap(enemies.majorBug.majorBugGroup, ballShieldPickup.shieldGroup, ballShieldPickup.onHitEnemy);

    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, ballShieldPickup.sprite, ballShieldPickup.onpickup);
        if(ballShieldPickup.sprite.y > game.width) {
            ballShieldPickup.sprite.kill();
            gameState.pickupSpawned = false;
            ballShieldPickup.spawned = false;
        }
    }
}