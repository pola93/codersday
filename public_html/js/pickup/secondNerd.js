/* 
 * Drugi nerd, czyli dochodzi do Ciebie dodatkowy obiekt gracza, który też strzela
 */

var secondNerdPickup = {
    name: 'secondNerd',
    startX: 0,
    startY: 0,
    duration: -1,
    graphicURL: graphicAssets.secondNerd.name,
    sprite: 0,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() {
        gameState.pickupSpawned = false;
        secondNerdPickup.spawned = false;
        secondNerdPickup.pickedUp=true;
        if(ship.numberOfNerds < 4) {
            ship.spawnNewNerd(100,100);
        }
        secondNerdPickup.sprite.kill();
    },
    

    spawn : function () {
        secondNerdPickup.spawned = true;
        secondNerdPickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, secondNerdPickup.graphicURL);
        game.physics.arcade.enable(secondNerdPickup.sprite, Phaser.Physics.ARCADE);
        secondNerdPickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, secondNerdPickup.sprite.body.velocity);
        
    },
    
    update : function () {
        secondNerdPickup.pickedUp = false;
    },
    
    destroy : function() {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, secondNerdPickup.sprite, secondNerdPickup.onpickup);
        if(secondNerdPickup.sprite.y > game.width) {
            secondNerdPickup.sprite.kill();
            secondNerdPickup.sprite.y=0;
            gameState.pickupSpawned = false;
            secondNerdPickup.spawned = false;
        }
    }
}