/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var coffeePickup = {
    name: 'coffee',
    startX: 0,
    startY: 0,
    duration: 5,
    graphicURL: graphicAssets.coffee.name,
    sprite: 0,
    shotIntervalDecrement : 90,
    shotSpeedIncrement : 300,
    speed : 300,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() { 
        gameState.pickupSpawned = false;
        coffeePickup.pickedUp = true;
        coffeePickup.spawned = false;
        coffeePickup.sprite.kill();
        ship.bulletProperties.speed += coffeePickup.shotSpeedIncrement;
        ship.bulletInterval -= coffeePickup.shotIntervalDecrement;
        game.time.events.add(Phaser.Timer.SECOND * coffeePickup.duration, coffeePickup.restoreBulletSpeed, gameState);
    },
    
    restoreBulletSpeed : function() {
        ship.bulletProperties.speed -= coffeePickup.shotSpeedIncrement;
        ship.bulletInterval += coffeePickup.shotIntervalDecrement;
        coffeePickup.pickedUp = false;
    },
    
    
    update : function () {
        
    },
    
    spawn : function () {
        coffeePickup.spawned = true;
        coffeePickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, coffeePickup.graphicURL);
        game.physics.arcade.enable(coffeePickup.sprite, Phaser.Physics.ARCADE);
        coffeePickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, coffeePickup.sprite.body.velocity);
        
    },
    
    destroy : function () {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, coffeePickup.sprite, coffeePickup.onpickup);
        if(coffeePickup.sprite.y > game.width) {
            coffeePickup.sprite.kill();
            coffeePickup.sprite.y=0;
            gameState.pickupSpawned = false;
            coffeePickup.spawned = false;
        }
    }
}