/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var shieldPickup = {
    name: 'shield',
    startX: 0,
    startY: 0,
    duration: -1,
    shiledIndicator:0,
    graphicURL: graphicAssets.shield.name,
    sprite: 0,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() {
        shieldPickup.spawned = false;
        shieldPickup.pickedUp = true;
        gameState.pickupSpawned = false;
        if(ship.shield < 1) {
            ship.shield++;
        }
        shieldPickup.shiledIndicator = game.add.sprite(0, 100, shieldPickup.graphicURL);
        shieldPickup.sprite.kill();
    },
    
    restoreShield : function() {
      ship.shipIsInvulnerable = false;
      shieldPickup.shiledIndicator.kill();
      shieldPickup.pickedUp = false;
    },
    
    spawn : function () {
        shieldPickup.spawned = true;
        shieldPickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, shieldPickup.graphicURL);
        game.physics.arcade.enable(shieldPickup.sprite, Phaser.Physics.ARCADE);
        shieldPickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, shieldPickup.sprite.body.velocity);
        
    },
    
    update : function () {
        shieldPickup.pickedUp = false;
    },
    
    destroy : function() {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, shieldPickup.sprite, shieldPickup.onpickup);
        if(shieldPickup.sprite.y > game.width) {
            shieldPickup.sprite.kill();
            shieldPickup.sprite.y=0;
            gameState.pickupSpawned = false;
            shieldPickup.spawned = false;
        }
    }
}