/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var extraLifePickup = {
    name: 'additionalMonitors',
    startX: 0,
    startY: 0,
    duration: -1,
    graphicURL: graphicAssets.extraLife.name,
    sprite: 0,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() { 
        extraLifePickup.spawned = false;
        extraLifePickup.pickedUp = true;
        if(ship.HP < 5) {
            ship.HP++;
            var hpGraph = ship.hpGraphicsGroup.getFirstExists(false);
            
            if (hpGraph) {
                hpGraph.reset(gameProperties.screenWidth - 100 * ship.HP, 100);
            }
        }
        extraLifePickup.sprite.kill();
    },
    
    spawn : function () {
        extraLifePickup.spawned = true;
        gameState.pickupSpawned = false;
        extraLifePickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, extraLifePickup.graphicURL);
        game.physics.arcade.enable(extraLifePickup.sprite, Phaser.Physics.ARCADE);
        extraLifePickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, extraLifePickup.sprite.body.velocity);
        
    },
    
    update : function () {
        extraLifePickup.pickedUp = false;
    },
    
    destroy : function() {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, extraLifePickup.sprite, extraLifePickup.onpickup);
        if(extraLifePickup.sprite.y > game.width) {
            extraLifePickup.sprite.kill();
            extraLifePickup.sprite.y=0;
            gameState.pickupSpawned = false;
            extraLifePickup.spawned = false;
        }
    }
}