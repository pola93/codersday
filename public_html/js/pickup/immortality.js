/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var immortalityPickup = {
    name: 'immortality',
    startX: 0,
    startY: 0,
    duration: 5,
    graphicURL: graphicAssets.immortality.name,
    sprite: 0,
    savedShieldNumber : 0,
    immortalityIndicator: 0,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() { 

        if(ship.shipIsInvulnerable) {
            return;
        }
        
        immortalityPickup.spawned = false;
        immortalityPickup.pickedUp = true;
        immortalityPickup.isImmortal = true;
        gameState.pickupSpawned = false;
        ship.shipIsInvulnerable = true;
        game.time.events.add(Phaser.Timer.SECOND * immortalityPickup.duration, immortalityPickup.restoreShield, gameState);
        immortalityPickup.immortalityIndicator = game.add.sprite(0,0, immortalityPickup.graphicURL);
        immortalityPickup.sprite.kill();
        
    },
    
    restoreShield : function() {
      ship.shipIsInvulnerable = false;
      immortalityPickup.immortalityIndicator.kill();
      immortalityPickup.pickedUp = false;
    },
    
    spawn : function () {
        immortalityPickup.spawned = true;
        immortalityPickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, immortalityPickup.graphicURL);
        game.physics.arcade.enable(immortalityPickup.sprite, Phaser.Physics.ARCADE);
        immortalityPickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, immortalityPickup.sprite.body.velocity);
        
    },
    
    update : function () {
        
    },
    
    destroy : function() {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, immortalityPickup.sprite, immortalityPickup.onpickup);
        if(immortalityPickup.sprite.y > game.width) {
            immortalityPickup.sprite.kill();
            immortalityPickup.sprite.y=0;
            gameState.pickupSpawned = false;
            immortalityPickup.spawned = false;
        }
    }
}