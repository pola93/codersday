/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var slowMotionPickup = {
    name: 'shield',
    startX: 0,
    startY: 0,
    duration: 5,
    graphicURL: 'assets/Shield.png',
    picked : false,
    bugStats : 0,
    blockerStats : 0,
    deadlineStats : 0,
    exceptionStats : 0,
    majorBugStats : 0,
    sprite: 0,
    timeDivider: 5,
    
    onpickup : function() {
        if(slowMotionPickup.picked) {
            return;
        }
        
        slowMotionPickup.picked = true;
        gameState.pickupSpawned = false;
        slowMotionPickup.sprite.kill();
        
        slowMotionPickup.bugStats.speed = enemies.bug.speed;
        slowMotionPickup.bugStats.shotSpeed = enemies.bug.bugShotSpeed;
        
        slowMotionPickup.deadlineStats.speed = enemies.deadline.speed;
        slowMotionPickup.deadlineStats.shotSpeed = enemies.deadline.deadlineShotSpeed;
        
        slowMotionPickup.exceptionStats.speed = enemies.exception.speed;
        slowMotionPickup.exceptionStats.shotSpeed = enemies.exception.exceptionShotSpeed;
        
        slowMotionPickup.majorBugStats.speed = enemies.majorBug.speed;
        slowMotionPickup.majorBugStats.shotSpeed = enemies.majorBug.majorBugShotSpeed;
        
        //zmniejszamy predkosc
        enemies.bug.speed /= slowMotionPickup.timeDivider;
       enemies.bug.bugShotSpeed /= slowMotionPickup.timeDivider;
        
        enemies.deadline.speed /= slowMotionPickup.timeDivider;
        enemies.deadline.deadlineShotSpeed /= slowMotionPickup.timeDivider;
        
        enemies.exception.speed /= slowMotionPickup.timeDivider;
        enemies.exception.exceptionShotSpeed /= slowMotionPickup.timeDivider;
        
        enemies.majorBug.speed /= slowMotionPickup.timeDivider;
        enemies.majorBug.majorBugShotSpeed /= slowMotionPickup.timeDivider;
        
        game.time.events.add(Phaser.Timer.SECOND * slowMotionPickup.duration, slowMotionPickup.restoreTime, gameState);
    },
    
    spawn : function () {
        slowMotionPickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, slowMotionPickup.graphicURL);
        game.physics.arcade.enable(slowMotionPickup.sprite, Phaser.Physics.ARCADE);
        slowMotionPickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, slowMotionPickup.sprite.body.velocity);
        
    },
    
    update : function () {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, slowMotionPickup.sprite, slowMotionPickup.onpickup);
        if(slowMotionPickup.sprite.y > game.width) {
            slowMotionPickup.sprite.kill();
            slowMotionPickup.sprite.y=0;
            gameState.pickupSpawned = false;
        }
    },
    
    restoreTime : function () {
        enemies.bug.speed = slowMotionPickup.bugStats.speed;
        enemies.bug.bugShotSpeed = slowMotionPickup.bugStats.shotSpeed;
        
        enemies.deadline.speed = slowMotionPickup.deadlineStats.speed;
        enemies.deadline.deadlineShotSpeed = slowMotionPickup.deadlineStats.shotSpeed;
        
        enemies.exception.speed = slowMotionPickup.exceptionStats.speed;
        enemies.exception.exceptionShotSpeed = slowMotionPickup.exceptionStats.shotSpeed;
        
        enemies.majorBug.speed = slowMotionPickup.majorBugStats.speed;
        enemies.majorBug.majorBugShotSpeed = slowMotionPickup.majorBugStats.shotSpeed;
        slowMotionPickup.picked = false;
    }
};
