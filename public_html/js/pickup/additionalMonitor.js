/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var additionalMonitorsPickup = {
    name: 'additionalMonitors',
    startX: 0,
    startY: 0,
    duration: -1,
    graphicURL: graphicAssets.extraMonitor.name,
    sprite: 0,
    spawned: false,
    pickedUp: false,
    
    onpickup : function() { 
        additionalMonitorsPickup.pickedUp = true;
        additionalMonitorsPickup.spawned = false;
        gameState.pickupSpawned = false;
        if(ship.numberOfMonitors < 3) {
            ship.numberOfMonitors++;
            switch(ship.numberOfMonitors){
                case 1:
                    ship.shipGroup.forEach(function (item) {
                        item.frameName = 'NerdSprite00';
                    });
                    break;
                case 2:
                    ship.shipGroup.forEach(function (item) {
                        item.frameName = 'NerdSprite01';
                    });
                    break;
                case 3:
                    ship.shipGroup.forEach(function (item) {
                        item.frameName = 'NerdSprite02';
                    });
                    break;
            }
        }
        additionalMonitorsPickup.sprite.kill();
    },
    
    spawn : function () {
        additionalMonitorsPickup.spawned = true;
        additionalMonitorsPickup.sprite = game.add.sprite(game.rnd.integerInRange(50, game.width - 50), 20, additionalMonitorsPickup.graphicURL);
        game.physics.arcade.enable(additionalMonitorsPickup.sprite, Phaser.Physics.ARCADE);
        additionalMonitorsPickup.sprite.body.setSize(57, 47, 2, 5);
        game.physics.arcade.velocityFromAngle(90, pickUpProperties.speed, additionalMonitorsPickup.sprite.body.velocity);
        
    },
    
    update : function () {
        additionalMonitorsPickup.pickedUp = false;
    },
    
    destroy : function() {
        
    },
    
    checkCollision : function () {
        game.physics.arcade.overlap(ship.shipGroup, additionalMonitorsPickup.sprite, additionalMonitorsPickup.onpickup);
        if(additionalMonitorsPickup.sprite.y > game.width) {
            additionalMonitorsPickup.sprite.kill();
            additionalMonitorsPickup.sprite.y=0;
            gameState.pickupSpawned = false;
            additionalMonitorsPickup.spawned = false;
        }
    }
}